import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class DateCalculatorTestUsingJUnit {

    @ParameterizedTest
    @MethodSource("dateDiffer")
    void testDateCalculator(MyDate fromDay, MyDate toDay, LocalDate dayFrom, LocalDate dayTo) {
        int expectedValue = (int) (dayFrom.until(dayTo, ChronoUnit.DAYS) - 1);
        CountDate resultCalculated = new DateCalculator(); // calling method 1 from DateCalculator
        System.out.println(expectedValue);
        assertEquals(expectedValue, resultCalculated.dateCounted(fromDay, toDay), "Method 1 no good");
    }

    @ParameterizedTest
    @MethodSource("dateDiffer")
    void testDateCalculator2(MyDate fromDay, MyDate toDay, LocalDate dayFrom, LocalDate dayTo) {
        int expectedValue = (int) (dayFrom.until(dayTo, ChronoUnit.DAYS) - 1);
        CountDate resultCalculated = new DateCalculator2(); // calling method 2 from DateCalculator2
        System.out.println(expectedValue);
        assertEquals(expectedValue, resultCalculated.dateCounted(fromDay, toDay), "Method 2 no good");
    }

    static Stream<Arguments> dateDiffer() {
        return Stream.of(
                arguments(new MyDate(1983, 6, 2), new MyDate(1983, 6, 22), LocalDate.of(1983, 6, 2), LocalDate.of(1983, 6, 22)),
                arguments(new MyDate(1984, 7, 4), new MyDate(1984, 12, 25), LocalDate.of(1984, 7, 4), LocalDate.of(1984, 12, 25)),
                arguments(new MyDate(989, 1, 3), new MyDate(1983, 8, 3), LocalDate.of(989, 1, 3), LocalDate.of(1983, 8, 3))

        );
    }

}
