import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.DAYS;

public class DateCalculatorTest  {



    public void testOne() throws Exception {
        CountDate dateCounted = new DateCalculator2();
        int resultCalculated = dateCounted.dateCounted(
                new MyDate(1983, 6, 2),
                new MyDate(1983, 6, 22)
        );
        Date dayFrom = new Date(1983,6,2);
        Date dayTo = new Date(1983,6,22);
        int dayDifference = (int)(TimeUnit.DAYS.convert(dayTo.getTime()- dayFrom.getTime()-1, TimeUnit.MILLISECONDS));
        //int dayDifference = dayTo.getDay()-dayFrom.getDay();
        if (resultCalculated == dayDifference) {
            System.out.println("test 1 right");
        } else {
            throw new Exception("test 1 wrong. "+ "expected: "+dayDifference+", received: " + resultCalculated);
        }
    }

    public void testTwo() throws Exception {
        CountDate dateCounted = new DateCalculator2();
        int resultCalculated = dateCounted.dateCounted(
                new MyDate(1984, 7, 4),
                new MyDate(1984, 12, 25)
        );
        LocalDate dayFrom = LocalDate.of(1984,7,4);
        LocalDate dayTo = LocalDate.of(1984, 12, 25);
        long dayDifference = (int)(DAYS.between(dayFrom,dayTo)-1);
        if (resultCalculated == dayDifference) {
            System.out.println("test 2 right");
        } else {
            throw new Exception("test 2 wrong. "+ "expected: "+dayDifference+", received: " + resultCalculated);
        }
    }

    public void testThree() throws Exception {
        CountDate dateCounted = new DateCalculator2();
        int resultCalculated = dateCounted.dateCounted(
                new MyDate(989, 1, 3),
                new MyDate(1983, 8, 3)
        );
        LocalDate dayFrom = LocalDate.of(989,1,3);
        LocalDate dayTo  = LocalDate.of(1983,8,3);
        int dayDifference =(int)(dayFrom.until(dayTo, ChronoUnit.DAYS)-1);
        if (resultCalculated == dayDifference) {
            System.out.println("test 3 right");
        } else {
            throw new Exception("test 3 wrong. "+ "expected: "+dayDifference+", received: " + resultCalculated);
        }
    }

    public void testFour() throws Exception {

        CountDate dateCounted = new DateCalculator();
        int resultCalculated = dateCounted.dateCounted(
                new MyDate(1983, 6, 2),
                new MyDate(1983, 6, 22)
        );
        LocalDate dayFrom = LocalDate.of(1983,6,2);
        LocalDate dayTo = LocalDate.of(1983,6,22);
        int dayDifference= (int)(dayFrom.until(dayTo, ChronoUnit.DAYS)-1);
        if (resultCalculated == dayDifference) {
            System.out.println("test 4 right");
        } else {
            throw new Exception("test 4 wrong. "+ "expected: "+dayDifference+", received: " + resultCalculated);
        }
    }

    public void testFive() throws Exception {
        CountDate dateCounted = new DateCalculator();
        int resultCalculated = dateCounted.dateCounted(
                new MyDate(1984, 7, 4),
                new MyDate(1984, 12, 25)
        );
        Date dayFrom = new Date(1984,7,4);
        Date dayTo = new Date(1984,12,25);
        int dayDifference = (int)(TimeUnit.DAYS.convert(dayTo.getTime() - dayFrom.getTime()-1, TimeUnit.MILLISECONDS));
        if (resultCalculated == dayDifference) {
            System.out.println("test 5 right");
        } else {
            throw new Exception("test 5 wrong. "+ "expected: "+dayDifference+", received: " + resultCalculated);
        }
    }

    public void testSix() throws Exception {
        CountDate dateCounted = new DateCalculator();
        int resultCalculated = dateCounted.dateCounted(
                new MyDate(989, 1, 3),
                new MyDate(1983, 8, 3)
        );
        Date dayFrom = new Date(989,1,3);
        Date dayTo = new Date(1983,8,3);
        int dayDifference = (int)(TimeUnit.DAYS.convert(dayTo.getTime() - dayFrom.getTime(), TimeUnit.MILLISECONDS)-1);
        if (resultCalculated == dayDifference) {
            System.out.println("test 6 right" );
        } else {
            throw new Exception("test 6 wrong. "+ "expected: "+dayDifference+", received: " + resultCalculated);
        }
    }


    public static void main(String[] args) {
        DateCalculatorTest test = new DateCalculatorTest();
        try {
            test.testOne();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            test.testTwo();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            test.testThree();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            test.testFour();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            test.testFive();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            test.testSix();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}


