import java.util.Scanner;

public class DateCalculator2 implements CountDate {


    public int dateCounted(MyDate dayFrom, MyDate dayTo) {


        int result = 0;
        int dayDifference = dayTo.getDay() - dayFrom.getDay();
        int monthDifference = 0;
        int yearDifference = (dayTo.getYear() - dayFrom.getYear()) * 365 + (dayTo.getYear() - dayFrom.getYear()) / 4 - (dayTo.getYear() - dayFrom.getYear()) * 3 / 400;
        //yearDifference is calculated by estimate the chance of getting a leap year inbetween.
        MyDate dateModifier = new MyDate(dayFrom.getYear(), (dayFrom.getMonth()), dayFrom.getDay());

        for (int i = 0; i < dayTo.getMonth() - dayFrom.getMonth(); i++) {
            monthDifference = monthDifference + dateModifier.daysInThisMonth();
            dateModifier = new MyDate(dateModifier.getYear(), (dateModifier.getMonth() + 1), dateModifier.getDay());
        }//monthDifference is based on daysInThisMonth method in Date class
        result = monthDifference + dayDifference + yearDifference;
        return --result;

    }
}
