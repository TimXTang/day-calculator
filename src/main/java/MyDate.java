import java.util.Scanner;

public class MyDate {

    private final int year;
    private final int month;
    private final int day;

    public MyDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public boolean leapYearOrNot() {
        if (year % 100 == 0 && year % 400 == 0) {
            return true;
        } else if(year % 4 == 0 && year % 100 !=0 ) {
            return true;
        } else {
            return false;
        }
    } // this is to determine whether the year is a leap year;

    public int daysInThisYear() {
        if (leapYearOrNot()) {
            return 366;
        } else {
            return 365;
        }
    } // yearIndex is used to indicate how many days are in that year
    public int daysInThisMonth() {
        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
            return 31;
        } else if (month == 4 || month == 6 || month == 9 || month == 11) {
            return 30;
        } else if (leapYearOrNot()) {
            return 29;
        } else {
            return 28;
        }
    }

    // monthIndex is used to indicate how many days are in that month

}
