public class DateCalculator implements CountDate {
//assume the dateTo is always in the future of dateFrom

    public int dateCounted(MyDate dateFrom, MyDate dateTo) {
        int result = 0;
        int dayDifference = dateTo.getDay() - dateFrom.getDay();
        int monthDifference = 0;
        int yearDifference = 0;
        MyDate dateModifier = new MyDate(dateFrom.getYear()+1, (dateFrom.getMonth()), dateFrom.getDay());
        for (int i = 0; i < dateTo.getMonth() - dateFrom.getMonth(); i++) {
            monthDifference = monthDifference + dateModifier.daysInThisMonth();
            dateModifier = new MyDate(dateModifier.getYear(), (dateModifier.getMonth() + 1), dateModifier.getDay());
        }// calculate the dates from Month Difference. Loop
        for (int j = 0; j < dateTo.getYear() - dateFrom.getYear(); j++) {
            yearDifference = yearDifference + dateModifier.daysInThisYear();
            dateModifier = new MyDate(dateModifier.getYear()+1, (dateModifier.getMonth()), dateModifier.getDay());
        }
        result = yearDifference + monthDifference + dayDifference;
        return --result;//return the value of result - 1 to int showDateCounted;

    }
}
